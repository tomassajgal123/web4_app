import React from 'react';
import {
    Image,
    Button,
    StyleSheet,
    TouchableOpacity,
    View
} from 'react-native';


import I18n from "react-native-i18n";

export default class menu extends React.Component {
    static navigationOptions = {
        header: null,
    };



    render() {
        return (

            <View style={styles.container} contentContainerStyle={styles.contentContainer}>
                <View style={styles.container1}>
                <Button  title={"SK"} onPress={sk}>SK</Button>
                <Button  title={"EN"} onPress={en}>EN</Button>
                </View>
                <Image
                    source={  require('../assets/images/uvodMA.png')}
                    style={styles.welcomeImage}
                />
                <TouchableOpacity style={styles.button} onPress={() => this.props.navigation.navigate('priklady')}>
                    <Image source={require('../assets/images/prik_button.png')}/>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} onPress={() => this.props.navigation.navigate('test')}>
                    <Image source={require('../assets/images/test_button.png')}/>
                </TouchableOpacity>
            </View>
        );
    }

}

function en() {
    I18n.locale = 'en';
}
function sk() {
    I18n.locale = 'sk';
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#222',
        justifyContent: 'space-between'
    },

    welcomeImage: {
        flex: 1,
        width: undefined,
        height: undefined,
        minWidth: 150,
        minHeight: 150,
        resizeMode: 'contain',
    },

    contentContainer: {
        alignItems: 'center',
        justifyContent: 'space-between'
    },

    button: {
        flexWrap: 'wrap',
        marginTop: 7,
        alignSelf: 'center',
    },
    container1: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },

});
