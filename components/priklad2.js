import React from 'react';
import {
    Image,
    Button,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import {strings} from "../locales/i18n";


export default class priklad1 extends React.Component {
    static navigationOptions = {
        header: null,
    };



    render() {
        return (
            <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
                <Text style={styles.textWhite}>{strings('priklady2.omacka')}</Text>
                <Text style={styles.textWhite}>{strings('priklady2.2')}</Text>
                <Text style={styles.textWhite}>{strings('priklady2.8')}</Text>
                <Text style={styles.textWhite}>{strings('priklady2.10')}</Text>
                <Text style={styles.textWhite}>{strings('priklady2.16')}</Text>
                <Text style={styles.textWhite}>{strings('priklady2.2a10a')}</Text>
                <Text style={styles.textWhite}>{strings('priklady2.2a10b')}</Text>
                <Text style={styles.textWhite}>{strings('priklady2.2a10c')}</Text>
                <Text style={styles.textWhite}>{strings('priklady2.2a10d')}</Text>
                <Text style={styles.textWhite}>{strings('priklady2.2a10e')}</Text>
                <Text style={styles.textWhite}>{strings('priklady2.2a10f')}</Text>
                <Text style={styles.textWhite}>{strings('priklady2.2a10g')}</Text>
                <Text style={styles.textWhite}>{strings('priklady2.2a10h')}</Text>
                <Text style={styles.textWhite}>{strings('priklady2.2a10i')}</Text>
                <Text style={styles.textWhite}>{strings('priklady2.2a10j')}</Text>

            </ScrollView>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#222',
    },
    textWhite:{
        alignSelf: 'flex-start',
        color: '#FFF',
        fontSize: 20,
        marginTop: 40,
    },

    contentContainer: {
        alignItems: 'center',
    },
    button: {
        color: '#ffffff',
        fontSize: 30,
        backgroundColor: '#0b0b55',
        marginTop: 7,
        width: 280,
        height: 100,
        borderColor: '#1312bb',
        borderWidth: 2,

    },
    buttonText: {
        fontSize: 37,
        textAlignVertical: 'center',
        textAlign: 'center',
        color: '#ffffff',
    },
});

