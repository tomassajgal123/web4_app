import React from 'react';
import {
    Image,
    Button,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import {strings} from "../locales/i18n";


export default class priklad1 extends React.Component {
    static navigationOptions = {
        header: null,
    };



    render() {
        return (
            <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
                <Text style={styles.textWhite}>{strings('priklady1.omacka')}</Text>
                <Text style={styles.textWhite}>{strings('priklady1.arit')}</Text>
                <Text style={styles.textWhite}>{strings('priklady1.arit1')}</Text>
                <Text style={styles.textWhite}>{strings('priklady1.arit2')}</Text>
                <Text style={styles.textWhite}>{strings('priklady1.arit3')}</Text>
                <Text style={styles.textWhite}>{strings('priklady1.arit4')}</Text>
                <Text style={styles.textWhite}>{strings('priklady1.logi')}</Text>
                <Text style={styles.textWhite}>{strings('priklady1.logi1')}</Text>
                <Text style={styles.textWhite}>{strings('priklady1.logi2')}</Text>
                <Text style={styles.textWhite}>{strings('priklady1.logi3')}</Text>
                <Text style={styles.textWhite}>{strings('priklady1.logi4')}</Text>
                <Text style={styles.textWhite}>{strings('priklady1.rotacie')}</Text>

            </ScrollView>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#222',
    },
    textWhite:{
        alignSelf: 'flex-start',
        color: '#FFF',
        fontSize: 20,
        marginTop: 40,
    },

    contentContainer: {
        alignItems: 'center',
    },
    button: {
        color: '#ffffff',
        fontSize: 30,
        backgroundColor: '#0b0b55',
        marginTop: 7,
        width: 280,
        height: 100,
        borderColor: '#1312bb',
        borderWidth: 2,

    },
    buttonText: {
        fontSize: 37,
        textAlignVertical: 'center',
        textAlign: 'center',
        color: '#ffffff',
    },
});

