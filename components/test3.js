import React from 'react';
import {
    Image,
    Button,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import { strings } from "../locales/i18n";
import SwitchSelector from 'react-native-switch-selector';

export default class test3 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            q1: "a",
            q2: "a",
            q3: "a",
            q4: "a",
            q5: "a",
            q6: "a",
            result: 0,
        };
    }

    static navigationOptions = {
        header: null,
    };

    _vyhodnotenie() {
        let p;
        p = this.state.result;
        let a = strings('test.test3.ans.q1');
        if (this.state.q1 === a) {
            p++;
        }

        a = strings('test.test3.ans.q2');
        if (this.state.q2 === a) {
            p++;
        }

        a = strings('test.test3.ans.q3');
        if (this.state.q3 === a) {
            p++;
        }

        a = strings('test.test3.ans.q4');
        if (this.state.q4 === a) {
            p++;
        }

        a = strings('test.test3.ans.q5');
        if (this.state.q5 === a) {
            p++;
        }

        a = strings('test.test3.ans.q6');
        if (this.state.q6 === a) {
            p++;
        }
        this.setState({ result: p });

        alert(
            'Vyhodnotenie:\n' +
            'Získali ste ' + p + ' bodov',
            [
                null,
                null,
                {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            { cancelable: false }
        )
        this.props.navigation.navigate('test')

    }

    render() {
        return (
            <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
                <View>

                    <Text style={styles.textWhite}>{strings('test.test3.q1.text')}</Text>
                    <View style={styles.ansverContainer}>
                        <Text style={styles.textWhiteSmall}>{strings('test.test3.q1.a1')}</Text>
                        <Text style={styles.textWhiteSmall}>{strings('test.test3.q1.a2')}</Text>
                        <Text style={styles.textWhiteSmall}>{strings('test.test3.q1.a3')}</Text>
                    </View>
                    <View>
                        <SwitchSelector
                            initial={0}
                            onPress={value => this.setState({ q1: value })}
                            textColor={'#7a44cf'} //'#7a44cf'
                            selectedColor={'#ffffff'}
                            buttonColor={'#7a44cf'}
                            borderColor={'#7a44cf'}
                            hasPadding
                            options={[
                                { label: 'a', value: 'a' },
                                { label: 'b', value: 'b' },
                                { label: 'c', value: 'c' }
                            ]} />
                    </View>



                    <Text style={styles.textWhite}>{strings('test.test3.q2.text')}</Text>
                    <Text style={styles.textWhiteSmall}>{strings('test.test3.q2.a1')}</Text>
                    <Text style={styles.textWhiteSmall}>{strings('test.test3.q2.a2')}</Text>
                    <Text style={styles.textWhiteSmall}>{strings('test.test3.q2.a3')}</Text>
                    <View>
                        <SwitchSelector
                            initial={0}
                            onPress={value => this.setState({ q2: value })}
                            textColor={'#7a44cf'} //'#7a44cf'
                            selectedColor={'#ffffff'}
                            buttonColor={'#7a44cf'}
                            borderColor={'#7a44cf'}
                            hasPadding
                            options={[
                                { label: 'a', value: 'a' },
                                { label: 'b', value: 'b' },
                                { label: 'c', value: 'c' }
                            ]} />
                    </View>



                    <Text style={styles.textWhite}>{strings('test.test3.q3.text')}</Text>
                    <Text style={styles.textWhiteSmall}>{strings('test.test3.q3.a1')}</Text>
                    <Text style={styles.textWhiteSmall}>{strings('test.test3.q3.a2')}</Text>
                    <Text style={styles.textWhiteSmall}>{strings('test.test3.q3.a3')}</Text>
                    <View>
                        <SwitchSelector
                            initial={0}
                            onPress={value => this.setState({ q3: value })}
                            textColor={'#7a44cf'} //'#7a44cf'
                            selectedColor={'#ffffff'}
                            buttonColor={'#7a44cf'}
                            borderColor={'#7a44cf'}
                            hasPadding
                            options={[
                                { label: 'a', value: 'a' },
                                { label: 'b', value: 'b' },
                                { label: 'c', value: 'c' }
                            ]} />
                    </View>



                    <Text style={styles.textWhite}>{strings('test.test3.q4.text')}</Text>
                    <Text style={styles.textWhiteSmall}>{strings('test.test3.q4.a1')}</Text>
                    <Text style={styles.textWhiteSmall}>{strings('test.test3.q4.a2')}</Text>
                    <Text style={styles.textWhiteSmall}>{strings('test.test3.q4.a3')}</Text>
                    <View>
                        <SwitchSelector
                            initial={0}
                            onPress={value => this.setState({ q4: value })}
                            textColor={'#7a44cf'} //'#7a44cf'
                            selectedColor={'#ffffff'}
                            buttonColor={'#7a44cf'}
                            borderColor={'#7a44cf'}
                            hasPadding
                            options={[
                                { label: 'a', value: 'a' },
                                { label: 'b', value: 'b' },
                                { label: 'c', value: 'c' }
                            ]} />
                    </View>



                    <Text style={styles.textWhite}>{strings('test.test3.q5.text')}</Text>
                    <Text style={styles.textWhiteSmall}>{strings('test.test3.q5.a1')}</Text>
                    <Text style={styles.textWhiteSmall}>{strings('test.test3.q5.a2')}</Text>
                    <Text style={styles.textWhiteSmall}>{strings('test.test3.q5.a3')}</Text>
                    <View>
                        <SwitchSelector
                            initial={0}
                            onPress={value => this.setState({ q5: value })}
                            textColor={'#7a44cf'} //'#7a44cf'
                            selectedColor={'#ffffff'}
                            buttonColor={'#7a44cf'}
                            borderColor={'#7a44cf'}
                            hasPadding
                            options={[
                                { label: 'a', value: 'a' },
                                { label: 'b', value: 'b' },
                                { label: 'c', value: 'c' }
                            ]} />
                    </View>


                    <Text style={styles.textWhite}>{strings('test.test3.q6.text')}</Text>
                    <Text style={styles.textWhiteSmall}>{strings('test.test3.q6.a1')}</Text>
                    <Text style={styles.textWhiteSmall}>{strings('test.test3.q6.a2')}</Text>
                    <Text style={styles.textWhiteSmall}>{strings('test.test3.q6.a3')}</Text>
                    <View>
                        <SwitchSelector
                            initial={0}
                            onPress={value => this.setState({ q6: value })}
                            textColor={'#7a44cf'} //'#7a44cf'
                            selectedColor={'#ffffff'}
                            buttonColor={'#7a44cf'}
                            borderColor={'#7a44cf'}
                            hasPadding
                            options={[
                                { label: 'a', value: 'a' },
                                { label: 'b', value: 'b' },
                                { label: 'c', value: 'c' }
                            ]} />
                    </View>

                    <TouchableOpacity
                        style={styles.button}
                        onPress={() => this._vyhodnotenie()}
                    >
                        <Text adjustsFontSizeToFit style={styles.buttonText}>{strings('test.vyhodnotenie')}</Text>
                    </TouchableOpacity>

                </View>
            </ScrollView>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#222',
    },
    textWhite:{
        alignSelf: 'center',
        color: '#FFF',
        fontSize: 30,
        marginTop: 40,
        marginLeft: 10,
    },
    textWhiteSmall:{
        textAlign: 'left',
        color: '#FFF',
        fontSize: 15,
        marginTop: 10,
        marginLeft: 10,
    },

    contentContainer: {
        alignItems: 'center',
    },
    ansverContainer: {
        marginBottom: 10,
    },
    button: {
        color: '#ffffff',
        fontSize: 30,
        backgroundColor: '#7a44cf',
        marginTop: 40,
        marginBottom: 20,
        marginLeft: 20,
        marginRight: 20,
        borderColor: '#ffffff',
        borderRadius: 25,
        padding: 5,

    },
    buttonText: {
        fontSize: 37,
        textAlignVertical: 'center',
        textAlign: 'center',
        color: '#ffffff',
    },
    vyhodnotenie: {
        fontSize: 40,
        backgroundColor: '#7a44cf',
        padding: 10,
        margin: 10,
    },
    vysledok: {
        display: null,
        alignSelf: 'center',
        color: '#FFF',
        fontSize: 30,
        marginTop: 40,
    }
});
