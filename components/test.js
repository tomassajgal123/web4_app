import React from 'react';
import {
    Image,
    Button,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import { strings } from "../locales/i18n"

export default class test extends React.Component {

    static navigationOptions = {
        header: null,
    };

    render() {
        return (
            <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
                <View>

                    <Text style={styles.textWhite}>{strings('test.text1')}</Text>

                    <TouchableOpacity
                        style={styles.button}
                        onPress={() => this.props.navigation.navigate('test1')}
                    >
                        <Text adjustsFontSizeToFit style={styles.buttonText}>{strings('test.button1')}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={styles.button}
                        onPress={() => this.props.navigation.navigate('test2')}
                    >
                        <Text adjustsFontSizeToFit style={styles.buttonText}>{strings('test.button2')}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={styles.button}
                        onPress={() => this.props.navigation.navigate('test3')}
                    >
                        <Text adjustsFontSizeToFit style={styles.buttonText}>{strings('test.button3')}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={styles.button}
                        onPress={() => this.props.navigation.navigate('vysledky')}
                    >
                        <Text adjustsFontSizeToFit style={styles.buttonText}>{strings('test.button4')}</Text>
                    </TouchableOpacity>

                </View>
            </ScrollView>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#222',
    },
    textWhite:{
        alignSelf: 'center',
        color: '#FFF',
        fontSize: 40,
        marginTop: 40,
    },

    contentContainer: {
        alignItems: 'center',
    },
    button: {
        color: '#ffffff',
        fontSize: 30,
        backgroundColor: '#0b0b55',
        marginTop: 7,
        borderColor: '#1312bb',
        borderWidth: 2,

    },
    buttonText: {
        fontSize: 37,
        textAlignVertical: 'center',
        textAlign: 'center',
        color: '#ffffff',
    },
});

