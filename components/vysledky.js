import React from 'react';
import {
    Image,
    Button,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import { strings } from "../locales/i18n"
import test from './test'

export default class vysledky extends test {
    constructor(props) {
        super(props);
    }

    static navigationOptions = {
        header: null,
    };

    render() {
        return (
                <View style={styles.container}>

                    <Text style={styles.textWhiteBig}>{strings('test.vyhodnotenie')}</Text>

                    <Text style={styles.textWhite}>{strings('test.vyh.text1')}</Text>
                    <Text style={styles.textWhiteSmall}>{strings('test.vyh.text4')  + strings('test.vyh.body')}</Text>

                    <Text style={styles.textWhite}>{strings('test.vyh.text2')}</Text>
                    <Text style={styles.textWhiteSmall}>{strings('test.vyh.text4') + strings('test.vyh.body')}</Text>

                    <Text style={styles.textWhite}>{strings('test.vyh.text3')}</Text>
                    <Text style={styles.textWhiteSmall}>{strings('test.vyh.text4') + strings('test.vyh.body')}</Text>

                </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#222',
    },
    textWhiteBig:{
        alignSelf: 'center',
        color: '#FFF',
        fontSize: 50,
        marginTop: 40,
        paddingLeft: 20,
        paddingRight: 20
    },
    textWhite:{
        alignSelf: 'center',
        color: '#FFF',
        fontSize: 28,
        marginTop: 40,
        paddingLeft: 20,
        paddingRight: 20
    },
    textWhiteSmall:{
        textAlign: 'left',
        color: '#FFF',
        fontSize: 20,
        marginTop: 20,
        marginLeft:20,
    },
    contentContainer: {
        alignItems: 'center',
    },
    button: {
        color: '#ffffff',
        fontSize: 30,
        backgroundColor: '#0b0b55',
        marginTop: 7,
        borderColor: '#1312bb',
        borderWidth: 2,

    },
    buttonText: {
        fontSize: 37,
        textAlignVertical: 'center',
        textAlign: 'center',
        color: '#ffffff',
    },
});

