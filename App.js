import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button} from 'react-native';


import {
    createStackNavigator,
    createAppContainer
} from 'react-navigation';
import welcome from './components/welcome.js'
import menu from './components/menu.js'
import priklady from './components/priklady.js'
import test from './components/test.js'
import test1 from './components/test1'
import test2 from './components/test2'
import test3 from './components/test3'
import vysledky from './components/vysledky'
import priklad1 from './components/priklad1.js'
import priklad2 from './components/priklad2.js'



const RootStack = createStackNavigator({
    welcome: {
        screen: welcome
    },
    menu: {
        screen: menu
    },
    test: {
        screen: test
    },
    test1: {
        screen: test1
    },
    test2: {
        screen: test2
    },
    test3: {
        screen: test3
    },
    vysledky: {
        screen: vysledky
    },
    priklady: {
        screen: priklady
    },
    priklad2: {
        screen: priklad2
    },
    priklad1: {
        screen: priklad1
    },
});





const App = createAppContainer(RootStack);

export default App;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});